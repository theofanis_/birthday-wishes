Birthday Wishes
===============

# Installation

```bash
git clone https://theofanis_@bitbucket.org/theofanis_/birthday-wishes.git
cd birthday-wishes
composer install
art storage:link
art app:install
```

 - Setup redis server
 - Setup horizon service
 - Edit `horizon.conf` and enable supervisor `sudo cp -f /var/www/birthday-wishes/horizon.conf /etc/supervisor/conf.d` and `sudo service supervisor restart`
 - Setup crontab `* * * * * cd /var/www/birthday-wishes && php artisan schedule:run >> /dev/null 2>&1`
 


