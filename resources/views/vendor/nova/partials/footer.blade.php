<p class="mt-8 text-center text-xs text-80">
    <a href="/" class="text-primary dim no-underline">{{ config('app.name') }}</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} <a href="https://theograms.tech" class="text-primary dim no-underline">Theograms</a>
    <span class="px-1">&middot;</span>
    v{{ \Laravel\Nova\Nova::version() }}
</p>
