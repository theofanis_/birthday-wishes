<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('clear:caches', function () {
    $this->call('clear-compiled');
    $this->call('cache:clear');
    $this->call('permission:cache-reset');
//    $this->call('page-cache:clear');
    $this->call('config:clear');
    $this->call('route:clear');
    $this->call('view:clear');
})->describe('Clear all application caches');

Artisan::command('ide-helper:all', function () {
    rescue(function () {
        $this->call('ide-helper:eloquent');
        $this->call('ide-helper:model', ['--reset' => true, '--write' => true]);
        $this->call('ide-helper:generate');
        $this->call('ide-helper:meta');
    });
})->describe('Call all ide-helper functions.');

Artisan::command('db:seed:roles-permissions', function () {
    $this->call('db:seed', ['--class' => 'DefaultRolesSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultPermissionsSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultRolesWithPermissionsSeeder', '--force' => true]);
    $this->call('cache:clear');
    $this->call('optimize');
})->describe('ReSeed permissions, roles and their associations');

Artisan::command('send:wish {client_id}', function ($client_id) {
    $client = \App\Models\Client::findOrFail($client_id);
    $this->line("Sending wish to {$client->name}");
    $client->notify(new \App\Notifications\BirthdayWish());
})->describe('Send BirthdayWish notification to client.');

Artisan::command('send:wish-now {client_id}', function ($client_id) {
    $client = \App\Models\Client::findOrFail($client_id);
    $this->line("Sending wish to {$client->name}");
    $notif = new \App\Notifications\BirthdayWish();
    $channels = array_filter($notif->via($client), 'class_exists');
    foreach ($channels as $channel) {
        $c = new $channel;
        $r = $c->send($client, null);
        dump($r);
    }
})->describe('Send BirthdayWish notification to client now.');
