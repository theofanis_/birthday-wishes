<?php
/**
 * wishes.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 1/7/20 12:16 π.μ.
 */

// this configuration will be override by the Setting

return [

    'enabled' => env('APP_ENV') == 'local',
    'message' => 'Χρόνια πολλά :name',
    'send_at' => '08:00',

    'sendinblue' => [
        'api_key' => env('SENDINBLUE_API_KEY', ''),
    ],

    'email' => [
        'subject' => 'Χρόνια Πολλά',
        'sender' => env('MAIL_FROM_ADDRESS', ''),
        'sender_name' => env('MAIL_FROM_NAME', ''),
    ],

    'sms' => [
        'sender_name' => env('MAIL_FROM_NAME', ''),
    ],

    'date_format' => 'Y-m-d',
];
