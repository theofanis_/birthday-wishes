<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Enable / Disable auto save
    |--------------------------------------------------------------------------
    |
    | Auto-save every time the application shuts down
    |
    */
    'auto_save' => true,

    /*
    |--------------------------------------------------------------------------
    | Cache
    |--------------------------------------------------------------------------
    |
    | Options for caching. Set whether to enable cache, its key, time to live
    | in seconds and whether to auto clear after save.
    |
    */
    'cache' => [
        'enabled' => true,
        'key' => 'setting',
        'ttl' => 3600,
        'auto_clear' => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Setting driver
    |--------------------------------------------------------------------------
    |
    | Select where to store the settings.
    |
    | Supported: "database", "json", "memory"
    |
    */
    'driver' => 'database',

    /*
    |--------------------------------------------------------------------------
    | Database driver
    |--------------------------------------------------------------------------
    |
    | Options for database driver. Enter which connection to use, null means
    | the default connection. Set the table and column names.
    |
    */
    'database' => [
        'connection' => env('DB_CONNECTION', 'mysql'),
        'table' => 'settings',
        'key' => 'key',
        'value' => 'value',
    ],

    /*
    |--------------------------------------------------------------------------
    | JSON driver
    |--------------------------------------------------------------------------
    |
    | Options for json driver. Enter the full path to the .json file.
    |
    */
    'json' => [
        'path' => storage_path('settings.json'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Override application config values
    |--------------------------------------------------------------------------
    |
    | If defined, settings package will override these config values.
    |
    | Sample:
    |   "app.locale" => "settings.locale",
    |
    */
    'override' => [
        'wishes.enabled' => 'wishes_enabled',
        'wishes.message' => 'wish_message',
        'wishes.send_at' => 'send_at',
        'wishes.sendinblue.api_key' => 'sendinblue_api_key',
        'wishes.email.subject' => 'email_subject',
        'wishes.email.sender_name' => 'sender_name',
        'wishes.email.sender' => 'email_sender',
        'wishes.sms.sender_name' => 'sender_name',
        'services.nexmo.sms_from' => 'sender_name',
        'nexmo.api_key' => 'nexmo_api_key',
        'nexmo.api_secret' => 'nexmo_api_secret',
    ],

    /*
    |--------------------------------------------------------------------------
    | Required Extra Columns
    |--------------------------------------------------------------------------
    |
    | The list of columns required to be set up
    |
    | Sample:
    |   "user_id",
    |   "tenant_id",
    |
    */
    'required_extra_columns' => [

    ],
];
