<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultUsersSeeder extends Seeder
{
    public function run()
    {
        $this->seedSuperAdmins();
        $this->seedAdmins();
        $this->seedOperators();
    }

    private function seedSuperAdmins()
    {
        $superAdmin = Role::superAdmin();
        User::system()->roles()->save($superAdmin);
        User::cron()->roles()->save($superAdmin);
        collect([
            ['name' => 'Theofanis', 'username' => 'theofanis', 'email' => 'vardtheo@gmail.com', 'plain_password' => 'Koukouts1.'],
        ])->each(function ($attrs) use ($superAdmin) {
            $u = User::create($attrs);
            $u->roles()->save($superAdmin);
        });
    }

    private function seedAdmins()
    {
        collect([
            ['name' => 'Διαχειριστής 1', 'email' => 'admin1@theograms.tech', 'plain_password' => '12345678'],
            ['name' => 'Σπύρος Σπυρόπουλος', 'email' => 'rebas313@gmail.com', 'plain_password' => 'Spyros.1', 'permissions' => ['super-admin']],
            ['name' => 'Παναγιώτης Σπυρόπουλος', 'email' => 'panosjseven@gmail.com', 'plain_password' => 'Panos.1', 'permissions' => ['super-admin']],
        ])->each(function ($attrs) {
            $permissions = array_pull($attrs, 'permissions');
            $u = User::create($attrs);
            if ($permissions) {
                $u->givePermissionTo($permissions);
            }
            $u->roles()->save(Role::admin());
        });
    }

    private function seedOperators()
    {
        collect([
            ['name' => 'Χειριστής 1', 'email' => 'operator1@theograms.tech', 'plain_password' => '12345678'],
            ['name' => 'Χειριστής 2', 'email' => 'operator2@theograms.tech', 'plain_password' => '12345678'],
        ])->each(function ($attrs) {
            $u = User::create($attrs);
            $u->roles()->save(Role::operator());
        });
    }
}
