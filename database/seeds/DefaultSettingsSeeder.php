<?php

use Illuminate\Database\Seeder;

class DefaultSettingsSeeder extends Seeder
{
    public function run()
    {
        Setting::set(collect(config('setting.override'))->flip()->map(function ($v, $k) {
            return config($v);
        })->toArray());
        Setting::save();
    }
}
