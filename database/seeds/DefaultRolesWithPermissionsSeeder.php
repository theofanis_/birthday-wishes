<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class DefaultRolesWithPermissionsSeeder extends Seeder
{
    /**
     * @var Role
     */
    protected $superAdmin;
    /**
     * @var Role
     */
    protected $admin;

    public function run()
    {
        $this->setSuperAdminPermissions();
        $this->setAdminPermissions();
        $this->setOperatorPermissions();
    }

    protected function setSuperAdminPermissions()
    {
        $this->superAdmin = Role::superAdmin()->syncPermissions(Permission::all());
    }

    protected function setAdminPermissions()
    {
        $permissions = Permission::findByPrefix('viewAny ActionEvent');
        $permissions->push(Permission::findByName('update-settings'));
        $permissions = $permissions->concat(Permission::where('name', 'like', 'SendPersonalMessage%')->get())
            ->concat(Permission::where('name', 'like', 'SendWishNow%')->get())
            ->concat(Permission::where('name', 'like', '%Note')->get())
            ->concat(Permission::where('name', 'like', '%Nameday')->get())
            ->concat(Permission::where('name', 'like', '%Client')->get())
            ->concat(Permission::where('name', 'like', '%ClientNotification')->get())
            ->concat(Permission::where('name', 'like', '%Role')->get())
            ->concat(Permission::where('name', 'like', '%User')->get());
        info('perms',$permissions->pluck('name')->toArray());
        $this->admin = Role::admin()->syncPermissions($permissions);
    }

    protected function setOperatorPermissions()
    {
        $permissions = Permission::where('name', 'like', '%Client%')->get();
        $permissions = $permissions->concat(Permission::where('name', 'like', '%Note')->get());
        Role::operator()->syncPermissions($permissions);
    }
}
