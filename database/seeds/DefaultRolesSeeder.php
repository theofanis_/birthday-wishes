<?php


use App\Models\Role;
use Illuminate\Database\Seeder;

class DefaultRolesSeeder extends Seeder
{
    public function run()
    {
        collect([
            ['name' => 'super-admin',],
            ['name' => 'admin',],
            ['name' => 'operator',],
        ])->each(function ($attrs) {
            Role::firstOrCreate($attrs);
        });
    }

}
