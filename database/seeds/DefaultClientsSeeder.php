<?php

use App\Models\Client;
use App\Models\Nameday;
use Illuminate\Database\Seeder;

class DefaultClientsSeeder extends Seeder
{
    public function run()
    {
        collect([
            ['first_name' => 'Θεοφάνης', 'description' => 'Server Developer', 'email' => 'vardtheo@gmail.com', 'phone' => '6934000909', 'contact_via_sms' => false, 'contact_via_email' => false, 'nameday' => 'Θεοφανείων'],
        ])->each(function (array $attrs) {
            $nameday = array_pull($attrs, 'nameday');
            $c = Client::create($attrs);
            if ($nameday) {
                $c->nameday()->associate(Nameday::findOrCreateByName($nameday));
                $c->save();
            }
        });
    }
}
