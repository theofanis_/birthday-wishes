<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class SystemUserSeeder extends Seeder
{
    public function run()
    {
        User::insert([
            'id'                => 1,
            'name'              => 'System',
            'username'          => 'system',
            'email'             => 'system@invoice-manager.com',
            'password'          => bcrypt('DataControlS'),
            'created_by'        => 1,
//            'user_type'         => 'system',
            'email_verified_at' => now(),
            'created_at'        => now(),
        ]);
        User::insert([
            'id'                => 2,
            'name'              => __('Cron Scheduler'),
            'username'          => 'cron',
            'email'             => 'cron@invoice-manager.com',
            'password'          => bcrypt('DataControlS'),
            'created_by'        => 1,
//            'user_type'         => 'system',
            'email_verified_at' => now(),
            'created_at'        => now(),
        ]);
    }
}
