<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(SystemUserSeeder::class);
        Auth::loginUsingId(User::system()->id);
        $this->call(DefaultRolesSeeder::class);
        $this->call(DefaultPermissionsSeeder::class);
        $this->call(DefaultRolesWithPermissionsSeeder::class);
        $this->call(DefaultUsersSeeder::class);
        $this->call(DefaultClientsSeeder::class);
        $this->call(DefaultSettingsSeeder::class);
        Auth::logout();
    }
}
