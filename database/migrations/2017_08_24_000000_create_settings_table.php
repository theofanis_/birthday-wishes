<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration
{
    public $table;
    public $keyColumn;
    public $valueColumn;

    public function __construct()
    {
        $this->table = config('setting.database.table');
        $this->keyColumn = config('setting.database.key');
        $this->valueColumn = config('setting.database.value');
    }

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string($this->keyColumn)->index();
            $table->text($this->valueColumn)->nullable();
        });
    }

    public function down()
    {
        Schema::drop($this->table);
    }
}
