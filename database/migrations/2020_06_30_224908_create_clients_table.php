<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            //excel columns
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('name')->virtualAs("TRIM( CONCAT(IFNULL(first_name,''),' ',IFNULL(last_name,'')) )");
            $table->string('gender')->nullable();
            $table->string('fathers_name')->nullable();
            $table->string('mothers_name')->nullable();
            $table->date('birthday')->nullable();
            $table->string('address')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('state')->nullable();
            $table->string('landline')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('description')->nullable();
            $table->string('kalikratis_vote')->nullable();
            $table->string('kapodistrias_vote')->nullable();

            $table->string('custom_message')->nullable();
            $table->json('contact_params');
            $table->boolean('important')->default(0);
            $table->foreignId('nameday_id')->nullable();
            $table->foreign('nameday_id')->references('id')->on('namedays');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('restrict');
        });
    }

    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
