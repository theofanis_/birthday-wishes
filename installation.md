
# Nginx

./nginx.conf

```
server {
    client_max_body_size 100M;
    ...
    location ~ .php$ {
        ...
        fastcgi_read_timeout 3600;
        
        fastcgi_param PHP_VALUE "upload_max_filesize=100M";
        fastcgi_param PHP_VALUE "post_max_size=100M";
        fastcgi_param PHP_VALUE "max_execution_time=3600";
        fastcgi_param PHP_VALUE "max_input_time=3600";
        fastcgi_param PHP_VALUE "memory_limit=1G";
        
        fastcgi_param PHP_VALUE "memory_limit=1G\nupload_max_filesize=100M\npost_max_size=100M\nmax_execution_time=3600\nmax_input_time=3600";
    }
}
```

# PHP 


```ini
max_execution_time = 3600
post_max_size = 100M
upload_max_filesize = 100M
max_input_time = 3600
```
