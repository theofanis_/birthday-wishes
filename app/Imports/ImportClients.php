<?php

namespace App\Imports;

use App\Models\Client;
use App\Models\Nameday;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportClients implements ToModel, WithHeadingRow, WithChunkReading
{
    use Importable;

    public function model(array $row)
    {
        return new Client([
            'kalikratis_vote'   => array_get($row, 'psifos_kalikratis'),
            'kapodistrias_vote' => array_get($row, 'psifos_kapodistrias'),
            'gender'            => array_get($row, 'sex'),
            'last_name'         => array_get($row, 'eponymo'),
            'first_name'        => array_get($row, 'onoma'),
            'fathers_name'      => array_get($row, 'patros'),
            'mothers_name'      => array_get($row, 'mitros'),
            'birthday'          => ($etgen = array_get($row, 'etgen')) ? Carbon::parse($etgen)->startOfYear() : null,
            'address'           => array_get($row, 'odos'),
            'zip'               => array_get($row, 'tk'),
            'state'             => array_get($row, 'perioxi'),
            'landline'          => array_get($row, 'telefon'),
            'phone'             => array_get($row, 'kinito'),
            'email'             => array_get($row, 'email'),
            'description'       => array_get($row, 'parathrhsis'),
            'nameday_id'        => ($o = array_get($row, 'eortastiko_onoma')) ? Nameday::findOrCreateByName($o)->id : null,
        ]);
    }

    public function chunkSize(): int
    {
        return 100;
    }

}
