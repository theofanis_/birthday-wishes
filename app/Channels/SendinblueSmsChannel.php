<?php

namespace App\Channels;

use App\Models\Client;
use App\Notifications\BirthdayWish;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class SendinblueSmsChannel
{
    public const TYPE = 'sms';

    /**
     * Send the given notification.
     *
     * @param Client                    $client
     * @param BirthdayWish|Notification $notification
     * @return \SendinBlue\Client\Model\SendSms
     */
    public function send($client, $notification)
    {
        // https://github.com/sendinblue/APIv3-php-library/blob/master/docs/Api/TransactionalSMSApi.md#sendtransacsms
        $config = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', config('wishes.sendinblue.api_key'));
        $apiInstance = new \SendinBlue\Client\Api\TransactionalSMSApi(null, $config);
        $sendTransacSms = new \SendinBlue\Client\Model\SendTransacSms([
            'sender'    => config('wishes.sms.sender_name'),
            'recipient' => $client->phone,
            'content'   => ($client->notification_message),
        ]);
        try {
            return $apiInstance->sendTransacSms($sendTransacSms);
        } catch (\Exception $e) {
            Log::critical("SendinblueSmsChannel exception: {$e->getMessage()}",$notification->toArray($client));
            throw $e;
        }
    }
}
