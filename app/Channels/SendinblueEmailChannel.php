<?php

namespace App\Channels;

use App\Models\Client;
use App\Notifications\PersonalMessage;
use App\Notifications\Wish;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class SendinblueEmailChannel
{
    public const TYPE = 'email';

    /**
     * Send the given notification.
     *
     * @param Client $client
     * @param Wish|Notification|PersonalMessage $notification
     * @return \SendinBlue\Client\Model\CreateSmtpEmail
     */
    public function send($client, $notification)
    {
        // https://github.com/sendinblue/APIv3-php-library/blob/master/docs/Api/SMTPApi.md#sendtransacemail
        $config = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', config('wishes.sendinblue.api_key'));
        $apiInstance = new \SendinBlue\Client\Api\SMTPApi(null, $config);
        $sendSmtpEmail = $notification->toSendinBlueEmail($client);
        try {
            return $apiInstance->sendTransacEmail($sendSmtpEmail);
        } catch (\Exception $e) {
            Log::critical("SendinblueEmailChannel exception: {$e->getMessage()}", $notification->toArray($client));
            throw $e;
        }
    }
}
