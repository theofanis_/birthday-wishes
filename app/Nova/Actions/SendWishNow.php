<?php

namespace App\Nova\Actions;

use App\Models\Client;
use App\Notifications\Wish;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class SendWishNow extends BaseAction
{
    use InteractsWithQueue, Queueable;

    protected $authorizeViaAbility = true;
//    public $showOnTableRow = true;

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|Client[] $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $client) {
            $client->notifyNow(new Wish());
        }
        return Action::message(__('Sent :count messages', ['count' => $models->count()]));
    }

    public function fields()
    {
        return [];
    }
}
