<?php

namespace App\Nova\Actions;

use Brightspot\Nova\Tools\DetachedActions\DetachedAction;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\File;

class ImportClients extends DetachedAction
{
//    use InteractsWithQueue, Queueable;

    public $showOnDetailToolbar = false;
    public $icon = 'hero-upload';
    public $iconClasses = 'mr-3 ';

    public function handle(ActionFields $fields, Collection $models)
    {
        Log::info('Importing clients via nova ImportClients');
        (new \App\Imports\ImportClients())->import(request()->file('file'));
        Log::info('Done importing clients via nova ImportClients');
    }

    public function fields()
    {
        return [
            File::make(__('File'), 'file')->required()
                ->help('Για μεγάλα αρχεία με 40.000 εγγραφές μπορεί να χρειαστούν ακόμη και μία ώρα!<br>Ενδείκνυνται να φορτώνετε αρχεία με 20.000 εγγραφές, που χρειάζονται ~20 λεπτά.<br>Μην σβήσετε την σελίδα όσο το εικονίδιο φορτώνει.'),
        ];
    }

    public function name()
    {
        return humanize($this);
    }
}
