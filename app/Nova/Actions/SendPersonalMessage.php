<?php

namespace App\Nova\Actions;

use App\Models\Client;
use App\Notifications\PersonalMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class SendPersonalMessage extends BaseAction
{
    use InteractsWithQueue, Queueable;

    protected $authorizeViaAbility = true;
//    public $showOnTableRow = true;

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|Client[] $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $message = $fields->get('message');
        $subject = $fields->get('subject');
        foreach ($models as $client) {
            $client->notifyNow(new PersonalMessage($message, $subject));
        }
        return Action::message(__('Sent :count messages', ['count' => $models->count()]));
    }

    public function fields()
    {
        return [
            Text::make(__('Email Subject'), 'subject')->required()
                ->help('Χρησιμοποιείται μόνο για τα μηνύματα μέσω email.'),
            Textarea::make(__('Message'), 'message')->required()
                ->help('Το μήνυμα θα σταλεί με τους επιλεγμένους τρόπους επικοινωνίας που έχουν οριστεί για κάθε πελάτη ξεχωριστά.'),
        ];
    }
}
