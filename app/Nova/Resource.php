<?php

namespace App\Nova;

use App\Nova\Management\User;
use DigitalCloud\NovaResourceNotes\Fields\Notes;
use Dillingham\NovaGroupedField\Grouped;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use Laravel\Nova\Panel;
use Laravel\Nova\Resource as NovaResource;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

abstract class Resource extends NovaResource
{
    public static $group = 'Other';

    public static $subtitle;

    /**
     * @var array|null Fields to be exported during DownloadExport. Must be visible in index page.
     */
    public static $export_fields = null;

    public function subtitle()
    {
        return static::$subtitle ? $this->{static::$subtitle} : null;
    }

    public static function group()
    {
        return __(Nova::humanize(static::$group == 'Other' ? static::getDirPath() : static::$group));
    }

    public static function getDirPath()
    {
        return str_before(str_after(static::class, 'App\\Nova\\'), '\\');
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder   $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query;
    }

    /**
     * Build a Scout search query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Laravel\Scout\Builder                  $query
     * @return \Laravel\Scout\Builder
     */
    public static function scoutQuery(NovaRequest $request, $query)
    {
        return $query;
    }

    /**
     * Build a "detail" query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder   $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function detailQuery(NovaRequest $request, $query)
    {
        return parent::detailQuery($request, $query);
    }

    /**
     * Build a "relatable" query for the given resource.
     *
     * This query determines which instances of the model may be attached to other resources.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder   $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function relatableQuery(NovaRequest $request, $query)
    {
        return parent::relatableQuery($request, $query);
    }

    /**
     * @param Request $request
     * @return Panel
     */
    protected function groupedBlameablePanel(Request $request)
    {
        $separator = '-';
        return new Panel(__('Timestamps') . " $separator " . __('Handlers'), [
            DateTime::make(__('Created At'), 'created_at')->sortable()->onlyOnIndex(),
//            DateTime::make(__('Updated At'), 'updated_at')->sortable()->onlyOnIndex(),
            DateTime::make(__('Deleted At'), 'deleted_at')->sortable()->onlyOnIndex()->canSee(function () {
                return $this->deleted_at;
            }),
            /***/
            Grouped::make(__('Created'))->fields([
                DateTime::make(__('Created At'), 'created_at'),
                BelongsTo::make(__('Creator'), 'creator', User::class),
            ])->hideFromIndex()->separator($separator),
            Grouped::make(__('Updated'))->fields([
                DateTime::make(__('Updated At'), 'updated_at'),
                BelongsTo::make(__('Updater'), 'updater', User::class),
            ])->hideFromIndex()->separator($separator),
            Grouped::make(__('Deleted'))->fields([
                DateTime::make(__('Deleted At'), 'deleted_at'),
                BelongsTo::make(__('Deleter'), 'deleter', User::class),
            ])->hideFromIndex()->separator($separator)->canSee(function () {
                return $this->deleted_at;
            }),
        ]);
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __(str_plural(static::resourceName()));
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __(static::resourceName());
    }

    public static function resourceName()
    {
        return Nova::humanize(class_basename(static::class));
    }

    /**
     * @return Notes
     */
    protected function notesField()
    {
        return Notes::make(__('Notes'), 'notes')->onlyOnDetail();
    }

    public function actions(Request $request)
    {
        return [
            (new DownloadExcel())->withHeadings(),
        ];
    }

    /**
     * @return DownloadExcel
     */
    public function actionDownloadExcel()
    {
        return tap((new DownloadExcel())->withHeadings(), function ($a) {
            if (static::$export_fields)
                $a->allFields()->only(static::$export_fields);
        });
    }

    public static function authorizedToCreate(Request $request)
    {
        return $request->user()->can('create ' . class_basename(static::$model));
    }

    public function authorizedToUpdate(Request $request)
    {
        return $request->user()->can('update ' . class_basename(static::$model));
    }

    public function authorizedToDelete(Request $request)
    {
        return $request->user()->can('delete ' . class_basename(static::$model));
    }

    public function authorizedToRestore(Request $request)
    {
        return $request->user()->can('restore ' . class_basename(static::$model));
    }

    public function authorizedToForceDelete(Request $request)
    {
        return false;
    }

    public function authorizedToView(Request $request)
    {
        return true;
    }

    public static function authorizedToViewAny(Request $request)
    {
        return $request->user()->can('viewAny ' . class_basename(static::$model));
    }

}
