<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Laravel\Nova\Filters\DateFilter;

class CreatedAt extends DateFilter
{
    public $column = 'created_at';

    public function __construct($column = 'created_at')
    {
        $this->column = $column;
    }

    /**
     * Apply the filter to the given query.
     *
     * @param \Illuminate\Http\Request              $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed                                 $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->whereDate($this->column,  Carbon::parse($value));
    }

    /**
     * Set the date column which will be queried.
     * @param $column
     * @return $this
     */
    public function column($column)
    {
        $this->column = $column;
        return $this;
    }

    public function name()
    {
        return humanize_attr($this->column);
    }

    public function key()
    {
        return get_class($this) . '-' . $this->column;
    }
}
