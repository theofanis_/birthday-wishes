<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class BooleanEnabled extends Filter
{
    /**
     * @var string name of column to query
     */
    public $column;

    public function __construct($name, $column)
    {
        $this->name = $name;
        $this->column = $column;
    }

    /**
     * Apply the filter to the given query.
     *
     * @param \Illuminate\Http\Request              $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed                                 $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where($this->column, (bool)$value);
    }

    public function options(Request $request)
    {
        return [
            __('Yes') => '1',
            __('No')  => '0',
        ];
    }

    public function key()
    {
        return get_class($this) . '-' . $this->column;
    }

}
