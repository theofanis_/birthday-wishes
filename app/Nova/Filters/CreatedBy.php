<?php

namespace App\Nova\Filters;

use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class CreatedBy extends Filter
{
//    public $component = 'typeahead-filter';

    /**
     * @var string column to query
     */
    public $column = 'created_by';

    /**
     * @param \Illuminate\Http\Request              $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed                                 $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where($this->column, $value);
    }

    public function options(Request $request)
    {
        return User::everyone()->pluck('id', 'name');
    }

    public function name()
    {
        return humanize_attr($this->column);
    }

    public function key()
    {
        return get_class($this) . '-' . $this->column;
    }
}
