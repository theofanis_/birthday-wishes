<?php

namespace App\Nova\Clients;

use App\Nova\Actions\ImportClients;
use App\Nova\Actions\SendPersonalMessage;
use App\Nova\Actions\SendWishNow;
use App\Nova\Filters\BirthdayAt;
use App\Nova\Filters\BooleanEnabled;
use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Filters\CreatedBy;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Sixlive\TextCopy\TextCopy;

class Client extends Resource
{
    public static $model = \App\Models\Client::class;

    public static $title = 'name';
    public static $subtitle = 'description';
    public static $search = [
        'id', 'first_name', 'last_name',  'description', 'email', 'phone',
//        'gender', 'fathers_name', 'mothers_name', 'address', 'zip_code', 'state', 'landline', 'kalikratis_vote', 'kapodistrias_vote',//PAID
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            //excel fields
//            Text::make(__('Full Name'), 'name')->exceptOnForms()->hideFromIndex(),
        //PAID fields ->sortable()
            Text::make(__('First Name'), 'first_name'),
            Text::make(__('Last Name'), 'last_name'),
            Text::make(__('Gender'), 'gender'),
            Text::make(__("Father's Name"), 'fathers_name'),
            Text::make(__("Mother's Name"), 'mothers_name'),
            Date::make(__('Birthday'), 'birthday')->sortable(),
            Text::make(__('Address'), 'address'),
            Text::make(__('Zip Code'), 'zip_code'),
            Text::make(__('State'), 'state'),
            Text::make(__('Landline'), 'landline'),
            TextCopy::make(__('Phone'), 'phone')->rules('nullable', 'digits_between:10,12')->help(__('Phone number with country code')),
            TextCopy::make(__('Email'), 'email')->rules('nullable', 'email'),
            Text::make(__('Kalikratis Vote'), 'kalikratis_vote'),
            Text::make(__('Kapodistrias Vote'), 'kapodistrias_vote'),
            Text::make(__('Description'), 'description'),

            Boolean::make(__('Important Client'), 'important'),
            Boolean::make(__('Contact via SMS'), 'contact_via_sms'),
            Boolean::make(__('Contact via Email'), 'contact_via_email'),
//            Textarea::make(__('Custom Message'), 'custom_message')->rules('max:254')->hideFromIndex(), //PAID
            Textarea::make(__('Notification Message'), 'notification_message')->exceptOnForms()->hideFromIndex(),
            BelongsTo::make(__('Nameday'), 'nameday', \App\Nova\Clients\Nameday::class)->nullable()->searchable()->showCreateRelationButton(),
            HasMany::make(__('Client Notifications'), 'client_notifications', \App\Nova\Clients\ClientNotification::class),
            $this->notesField(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [
            new BirthdayAt(),
            new BooleanEnabled(__('Important Client'), 'important'),
//            new BooleanEnabled(__('Contact via SMS'), 'contact_params->via_sms'), //PAID
//            new BooleanEnabled(__('Contact via Email'), 'contact_params->via_email'), //PAID
            new CreatedBefore(),
            new CreatedAfter(),
            new CreatedBy(),
        ];
    }


    public function actions(Request $request)
    {
        return [
            new SendWishNow(),
            new SendPersonalMessage(),
            new ImportClients(),
        ];
    }
}
