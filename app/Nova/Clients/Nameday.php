<?php

namespace App\Nova\Clients;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use NovaItemsField\Items;

class Nameday extends Resource
{
    public static $model = \App\Models\Nameday::class;
    public static $title = 'name';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'name','code', 'celebration_dates',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')->sortable(),
            Slug::make(__('Code'),'code')->from('name')->readonly()->onlyOnDetail(),
            Items::make(__('Celebration Dates'), 'celebration_dates')
                ->placeholder(__('Add celebration date'))
                ->createButtonValue(__('Add'))
                ->draggable()
                ->inputType('date')
                ->rules([
                    'celebration_dates.*' => 'date_format:' . config('wishes.date_format'),
                ])
                ->help(__('Example celebration date') . ': ' . now()->format(config('wishes.date_format'))),
            HasMany::make(__('Clients'), 'clients', \App\Nova\Clients\Client::class),
            $this->groupedBlameablePanel($request),
            $this->notesField(),
        ];
    }

}
