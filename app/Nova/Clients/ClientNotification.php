<?php

namespace App\Nova\Clients;

use App\Nova\Filters\BooleanEnabled;
use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Sixlive\TextCopy\TextCopy;

class ClientNotification extends Resource
{
    public static $model = \App\Models\ClientNotification::class;
    public static $globallySearchable = false;
    public static $title = 'id';
    public static $search = [
        'id', 'phone', 'email',
    ];
    public static $with = ['client'];

    public function fields(Request $request)
    {
        return [
            ID::make(),
            Text::make(__('Message'), 'message'),
            TextCopy::make(__('Phone'), 'phone'),
            TextCopy::make(__('Email'), 'email'),
            Boolean::make(__('SMS'), 'contact_via_sms'),
            Boolean::make(__('Email'), 'contact_via_email'),
            BelongsTo::make(__('Client'), 'client', \App\Nova\Clients\Client::class),
//            DateTime::make(__('Read At'), 'read_at')->sortable()->onlyOnDetail(),
            Text::make(__('Type'), 'type')->canSeeWhen('super-admin'),
            Code::make(__('Data'), 'data')->json(JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)->canSeeWhen('super-admin'),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [
            new BooleanEnabled(__('Contact via SMS'), 'contact_params->via_sms'),
            new BooleanEnabled(__('Contact via Email'), 'contact_params->via_email'),
            new CreatedBefore(),
            new CreatedAfter(),
        ];
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

}
