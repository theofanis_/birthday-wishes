<?php

namespace App\Nova\Management;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;

class User extends Resource
{
    public static $model = \App\Models\User::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = [
        'id', 'name', 'username', 'email',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Gravatar::make()->maxWidth(50),
            Text::make(__('Name'), 'name')->sortable()->rules('required', 'max:255'),
            Text::make(__('Username'), 'username')
                ->sortable()
                ->rules('required', 'max:254')
                ->creationRules('unique:users,username')
                ->updateRules('unique:users,username,{{resourceId}}'),
            Text::make(__('Email'), 'email')->sortable()->rules('required', 'email', 'max:254')->creationRules('unique:users,email')->updateRules('unique:users,email,{{resourceId}}'),
            Text::make(__('API Token'), 'api_token')->exceptOnForms()->hideFromIndex(),
            Password::make(__('Password'), 'password')->onlyOnForms()->creationRules('required', 'string', 'min:8')->updateRules('nullable', 'string', 'min:8'),
            MorphToMany::make(__('Roles'), 'roles', \App\Nova\Management\Role::class),
            MorphToMany::make(__('Permissions'), 'permissions', \App\Nova\Management\Permission::class),
            $this->groupedBlameablePanel($request),
            $this->notesField(),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }

}
