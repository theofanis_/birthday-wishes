<?php

namespace App\Nova\Management;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class Permission extends Resource
{
    public static $model = \App\Models\Permission::class;

    public static $globallySearchable = false;

    public static $title = 'name';

    public static $search = [
        'id', 'name',
    ];

    public function fields(Request $request)
    {
        $guardOptions = collect(config('auth.guards'))->mapWithKeys(function ($value, $key) {
            return [$key => $key];
        });
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')
                ->rules(['required', 'string', 'max:255'])
                ->creationRules('unique:' . config('permission.table_names.permissions'))
                ->updateRules('unique:' . config('permission.table_names.permissions') . ',name,{{resourceId}}'),
            Select::make(__('Guard Name'), 'guard_name')
                ->options($guardOptions->toArray())
                ->rules(['required', Rule::in($guardOptions)]),
            BelongsToMany::make(__('Roles'), 'roles', \App\Nova\Management\Role::class),
            BelongsToMany::make(__('Users'), 'users', \App\Nova\Management\User::class),
            $this->groupedBlameablePanel($request),
        ];
    }

}
