<?php

namespace App\Notifications;

use App\Channels\SendinblueEmailChannel;
use App\Models\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class PersonalMessage extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var string
     */
    public $message;
    /**
     * @var string
     */
    public $subject;

    /**
     * @param string $message
     * @param string|null $subject
     */
    public function __construct($message, $subject)
    {
        $this->message = $message;
        $this->subject = $subject;
    }

    /**
     * @param Client $client
     * @return array
     */
    public function via($client)
    {
        return array_keys(array_filter(['nexmo' => $client->contact_via_sms, SendinblueEmailChannel::class => $client->contact_via_email, 'database' => true]));
    }

    /**
     * Get the Vonage / SMS representation of the notification.
     * @param Client $client
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmo($client)
    {
        return (new NexmoMessage)
            ->content($this->message)
            ->unicode();
    }

    /**
     * @param $client
     * @return \SendinBlue\Client\Model\SendSmtpEmail
     */
    public function toSendinBlueEmail($client)
    {
        return new \SendinBlue\Client\Model\SendSmtpEmail([
            'sender'      => new \SendinBlue\Client\Model\SendSmtpEmailSender(['email' => config('wishes.email.sender'), 'name' => config('wishes.email.sender_name')]),
            'to'          => [new \SendinBlue\Client\Model\SendSmtpEmailTo(['email' => $client->email, 'name' => $client->name])],
            'subject'     => $this->subject,
            'textContent' => $this->message,
//            'htmlContent' => '',
        ]);
    }

    /**
     * @param Client $client
     * @return array
     */
    public function toArray($client)
    {
        return [
            'message' => $this->message,
        ];
    }
}
