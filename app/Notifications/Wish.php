<?php

namespace App\Notifications;

use App\Channels\SendinblueEmailChannel;
use App\Channels\SendinblueSmsChannel;
use App\Models\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class Wish extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @param Client $client
     * @return array
     */
    public function via($client)
    {
        return (config('wishes.enabled') || (static::class == self::class)) ? array_keys(array_filter(['nexmo' => $client->contact_via_sms, SendinblueEmailChannel::class => $client->contact_via_email, 'database' => true])) : [];
    }

    /**
     * Get the Vonage / SMS representation of the notification.
     * @param  Client  $client
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmo($client)
    {
        return (new NexmoMessage)
            ->content($client->notification_message)
            ->unicode();
    }

    /**
     * @param $client
     * @return \SendinBlue\Client\Model\SendSmtpEmail
     */
    public function toSendinBlueEmail($client)
    {
        return new \SendinBlue\Client\Model\SendSmtpEmail([
            'sender'      => new \SendinBlue\Client\Model\SendSmtpEmailSender(['email' => config('wishes.email.sender'), 'name' => config('wishes.email.sender_name')]),
            'to'          => [new \SendinBlue\Client\Model\SendSmtpEmailTo(['email' => $client->email, 'name' => $client->name])],
            'subject'     => config('wishes.email.subject'),
            'textContent' => $client->notification_message,
//            'htmlContent' => '',
        ]);
    }

    /**
     * @param Client $c
     * @return array
     */
    public function toArray($c)
    {
        return $c->toArray();
    }
}
