<?php

namespace App\Listeners;

use App\Models\ClientNotification;
use Illuminate\Notifications\Events\NotificationSent;

class LogNotification
{

    public function handle(NotificationSent $event)
    {
        if (class_exists($event->channel))
            try {
                ClientNotification::find($event->notification->id)->update([$event->channel::TYPE . "_response" => $event->response]);
            } catch (\Throwable $e) {
                report($e);
            }
    }
}
