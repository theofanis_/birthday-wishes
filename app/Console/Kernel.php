<?php

namespace App\Console;

use App\Console\Commands\SendTodayBirthdayWishes;
use App\Console\Commands\SendTodayNamedayWishes;
use App\Models\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('backup:clean')->daily()->at('01:00');
        $schedule->command('backup:run')->daily()->at('02:00');
        $schedule->command('telescope:prune --hours=120')->daily();
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
        $schedule->command('backup:run')->skip(App::environment('local'))->daily()->at('02:00');
        $schedule->command('backup:clean')->skip(App::environment('local'))->daily()->at('03:00');
        $schedule->command('backup:monitor')->skip(App::environment('local'))->daily()->at('04:00');
        $schedule->command(SendTodayBirthdayWishes::class)->before(function () {
            Auth::login(User::cron());
        })->dailyAt(config('wishes.send_at'));
        $schedule->command(SendTodayNamedayWishes::class)->before(function () {
            Auth::login(User::cron());
        })->dailyAt(config('wishes.send_at'));
    }

    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
