<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\User;
use App\Notifications\BirthdayWish;
use Illuminate\Console\Command;

class SendTodayBirthdayWishes extends Command
{
    protected $signature = 'send:today:birthday-wishes';

    protected $description = 'Send wishes to all clients with birthday today.';

    public function handle()
    {
        while_logged_in(function () {
            Client::birthdayAt(now())->get()->each(function (Client $client) {
                $this->line("Sending birthday wish to {$client->name} #{$client->id}");
                $client->notifySingle(new BirthdayWish());
            });
        }, User::system());
        return 0;
    }

}
