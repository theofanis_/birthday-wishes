<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\User;
use App\Notifications\BirthdayWish;
use App\Notifications\NamedayWish;
use Illuminate\Console\Command;

class SendTodayNamedayWishes extends Command
{
    protected $signature = 'send:today:nameday-wishes';

    protected $description = 'Send wishes to clients that have their nameday today.';

    public function handle()
    {
        while_logged_in(function () {
            Client::namedayAt(now())->get()->each(function (Client $client) {
                $this->line("Sending nameday wish to {$client->name} #{$client->id}");
                $client->notifySingle(new NamedayWish());
            });
        }, User::system());
        return 0;
    }
}
