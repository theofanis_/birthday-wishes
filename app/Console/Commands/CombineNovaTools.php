<?php
/**
 * CombineNovaTools.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 20/2/20 4:46 μ.μ.
 */

namespace App\Console\Commands;

use App\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Laravel\Nova\Nova;

class CombineNovaTools extends Command
{
    protected $signature = 'nova:combine-tools {--r|remove : Delete existing combined files.} {--p|preview : Preview files as they are combined.}';

    protected $description = 'Combines all nova tools scripts and styles in nova-tools.[js|css] in public/[js/css]';

    public function handle()
    {
        $this->option('remove') ? $this->deleteFiles() : $this->combineTools();
    }

    private function combineTools()
    {
        // https://github.com/laravel/nova-issues/issues/1146#issuecomment-515891323
        $this->info('Combining nova tools js and css');
        Auth::login(Role::superAdmin()->users()->first());
        app()->handle(Request::create(Nova::path()));
        $display = $this->option('preview');
        foreach (['allScripts' => 'js', 'allStyles' => 'css'] as $method => $type) {
            $content = '';
//            $content = "/*--- " . now()->toDateTimeString() . " ---*/\n";
            foreach (($files = Nova::{$method}()) as $name => $file) {
                if ($display)
                    $this->line("$name -> $file " . filesize_formatted($file));
                $content .= "/*-- $name --*/\n" . \file_get_contents($file) . "\n";
            }
            $dest = public_path('vendor/nova/tools.' . $type);
            \file_put_contents($dest, $content);
            $this->info("Combined " . count($files) . " files " . filesize_formatted($dest) . " to $dest");
        }
    }

    protected function deleteFiles()
    {
        foreach (['allScripts' => 'js', 'allStyles' => 'css'] as $method => $type) {
            $dest = public_path('vendor/nova/tools.' . $type);
            if (file_exists($dest)) {
                unlink($dest);
                $this->info("Deleted $dest");
            } else {
                $this->warn("File does not exists $dest");
            }
        }
    }
}
