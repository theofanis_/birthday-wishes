<?php

namespace App\Console\Commands;

use App\Models\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\HeadingRowImport;

class TestImportClients extends Command
{
    protected $signature = 'test:import {--c|create=40000 : Create demo csv with 40000 records} {file?}';

    protected $description = 'Test import of many clients.';

    public function handle()
    {
        $path = $file ?? base_path('demo-import.csv');
        if ($this->option('create') || !File::exists($path)) {
            $this->line('Creating demo csv...');
            $headers = (new HeadingRowImport)->toArray(base_path('example-import.csv'))[0][0];
            File::put($path, implode(',', $headers) . "\n");
            $records = $this->option('create') ?: 40000;
            $bar = $this->output->createProgressBar($records);
            $bar->start();
            for ($i = 1; $i <= $records; $i++) {
                $bar->advance();
                $data = [
                    "psifos_kalikratis"   => "psifos_kalikratis $i",
                    "psifos_kapodistrias" => "psifos_kapodistrias $i",
                    "sex"                 => "sex $i",
                    "eponymo"             => "eponymo $i",
                    "onoma"               => "onoma $i",
                    "patros"              => "patros $i",
                    "mitros"              => "mitros $i",
                    "etgen"               => rand(1960, 2000),
                    "odos"                => "odos $i",
                    "tk"                  => "$i",
                    "perioxi"             => "perioxi $i",
                    "telefon"             => "telefon $i",
                    "kinito"              => "",
                    "email"               => "$i@mail.com",
                    "parathrhsis"         => "parathrhsis $i",
                    "eortastiko_onoma"    => "eortastiko_onoma $i",
                ];
                $data = implode(',', $data);
                File::append($path, "$data\n");
            }
            $bar->finish();
            $this->line("");
            $this->line("Created $records -> $path");
        }
        $this->line('Clearing clients');
        Client::query()->forceDelete();
        Auth::loginUsingId(1);
        $this->line("[" . ($b = now())->toTimeString() . "] Importing clients...");
        (new \App\Imports\ImportClients())->import($path);
        $this->line("[" . ($e = now())->toTimeString() . "] Imported " . Client::count() . " clients. (" . $e->diffInRealMinutes($b) . " minutes)");
        return 0;
    }
}
