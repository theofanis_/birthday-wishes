<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel as ConsoleKernelContract;

class AppInstall extends Command
{
    protected $signature = 'app:install {--r|reset : Drop tables}';

    protected $description = 'Create database, seed default models & configuration, clear caches';

    public function handle()
    {
        $this->call('clear:caches');
        $this->getFreshConfiguration();
        $reset = $this->option('reset');
        if ($reset) {
            $this->resetApp();
        }
        $this->getFreshConfiguration();
        $this->call('migrate');
        $this->call('db:seed');
        $this->call('nova:combine-tools');
    }

    protected function resetApp()
    {
        if (app()->environment() != 'local') {
            $this->call('snapshot:create', ['name' => 'reset-' . now()->format('Y-m-d_H-i-s')]);
        }
        $this->dropDB('mysql');
        try {
            $this->dropDB('telescope');
        }catch (\Exception $e){
            //
        }
    }

    protected function dropDB($database)
    {
        $this->laravel['db']->connection($database)
            ->getSchemaBuilder()
            ->dropAllTables();
        $this->laravel['db']->connection($database)
            ->getSchemaBuilder()
            ->dropAllViews();
        $this->info("Dropped all tables/views successfully on $database.");
    }

    protected function getFreshConfiguration($load = true)
    {
        $app = require $this->laravel->bootstrapPath() . '/app.php';
        $app->make(ConsoleKernelContract::class)->bootstrap();
        if ($load) {
            config($app['config']->all());
        }
        return $app['config']->all();
    }

}
