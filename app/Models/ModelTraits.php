<?php
/**
 * ModelTraits.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 29/9/20 1:53 PM
 */

namespace App\Models;


use DigitalCloud\ModelNotes\HasNotes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Nova\Actions\Actionable;
use RichanFongdasen\EloquentBlameable\BlameableService;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

trait ModelTraits
{
    use SoftDeletes;
    use Actionable, HasNotes;
    use BlameableTrait;

    public function deleter(): BelongsTo
    {
        return $this->belongsTo(
            app(BlameableService::class)->getConfiguration($this, 'user'),
            app(BlameableService::class)->getConfiguration($this, 'deletedBy')
        );
    }

}
