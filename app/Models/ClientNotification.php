<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\DatabaseNotification;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * App\Models\ClientNotification
 *
 * @property string $id
 * @property string $type
 * @property string $notifiable_type
 * @property int $notifiable_id
 * @property array $data
 * @property \Illuminate\Support\Carbon|null $read_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\User|null $creator
 * @property-read bool $contact_via_email
 * @property-read bool $contact_via_sms
 * @property-read string $email
 * @property string|null $mail_response
 * @property-read string $message
 * @property-read string $phone
 * @property string|null $sms_response
 * @property-read string $wish_type
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $notifiable
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Notifications\DatabaseNotificationCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification createdBy($userId)
 * @method static \Illuminate\Notifications\DatabaseNotificationCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification newQuery()
 * @method static \Illuminate\Database\Query\Builder|ClientNotification onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereNotifiableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereNotifiableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientNotification whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|ClientNotification withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ClientNotification withoutTrashed()
 * @mixin \Eloquent
 */
class ClientNotification extends DatabaseNotification
{
    use SoftDeletes, BlameableTrait;

    protected $fillable = ['id', 'type', 'data', 'response', 'read_at', 'sms_response', 'mail_response'];

    /**
     * @return string
     */
    public function getMessageAttribute()
    {
        return array_get($this->data, 'message');
    }

    /**
     * @return string
     */
    public function getEmailAttribute()
    {
        return array_get($this->data, 'email');
    }

    /**
     * @return string
     */
    public function getPhoneAttribute()
    {
        return array_get($this->data, 'phone');
    }

    /**
     * @return string
     */
    public function getWishTypeAttribute()
    {
        return array_get($this->data, 'wish_type');
    }

    /**
     * @return bool
     */
    public function getContactViaSmsAttribute()
    {
        return array_get($this->data, 'contact_via_sms');
    }

    /**
     * @return bool
     */
    public function getContactViaEmailAttribute()
    {
        return array_get($this->data, 'contact_via_email');
    }

    /**
     * @return string|null
     */
    public function getMailResponseAttribute()
    {
        return array_get($this->data, 'mail_response');
    }

    /**
     * @param string|null $value
     * @return string|null
     */
    public function setMailResponseAttribute($value)
    {
        $p = $this->data;
        $this->data = array_set($p, 'mail_response', $value);
        return $this->mail_response;
    }

    /**
     * @return string|null
     */
    public function getSmsResponseAttribute()
    {
        return array_get($this->data, 'sms_response');
    }

    /**
     * @param string|null $value
     * @return string|null
     */
    public function setSmsResponseAttribute($value)
    {
        $p = $this->data;
        $this->data = array_set($p, 'sms_response', $value);
        return $this->sms_response;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'notifiable_id');
    }

}
