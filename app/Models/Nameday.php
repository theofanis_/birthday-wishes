<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Nameday
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property array $celebration_dates
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User $deleter
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday query()
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereCelebrationDates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nameday whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Nameday extends Model
{
    use ModelTraits;

    protected $fillable = ['name', 'code'];
    protected $attributes = [
        'celebration_dates' => '[]',
    ];
    protected $casts = [
        'celebration_dates' => 'array',
    ];

    protected static function booted()
    {
        static::creating(function (Nameday $nameday) {
            if ($nameday->isDirty('name')) {
                $nameday->code = str_slug($nameday->name);
            }
        });
    }

    /**
     * @param string $name
     * @return Nameday|null
     */
    public static function findByName($name)
    {
        return static::where('name', 'like', $name)
            ->orWhere('code', 'like', str_slug($name))
            ->first();
    }

    /**
     * @param $name
     * @return Nameday
     */
    public static function findOrCreateByName($name)
    {
        return self::findByName($name) ?? static::create(['name' => $name]);
    }

    public function clients()
    {
        return $this->hasMany(Client::class);
    }

}
