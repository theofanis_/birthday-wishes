<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $name
 * @property string|null $gender
 * @property string|null $fathers_name
 * @property string|null $mothers_name
 * @property Carbon|null $birthday
 * @property string|null $address
 * @property string|null $zip_code
 * @property string|null $state
 * @property string|null $landline
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $description
 * @property string|null $kalikratis_vote
 * @property string|null $kapodistrias_vote
 * @property string|null $custom_message
 * @property array $contact_params
 * @property int $important
 * @property int|null $nameday_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\App\Models\ClientNotification[] $client_notifications
 * @property-read int|null $client_notifications_count
 * @property-read \App\Models\User $creator
 * @property-read \App\Models\User|null $deleter
 * @property bool $contact_via_email
 * @property bool $contact_via_sms
 * @property-read string $notification_message
 * @property-read \App\Models\Nameday|null $nameday
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\App\Models\ClientNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\User|null $updater
 * @method static Builder|Client birthdayAt($date = 'today')
 * @method static Builder|Client createdBy($userId)
 * @method static Builder|Client namedayAt($date = 'today')
 * @method static Builder|Client newModelQuery()
 * @method static Builder|Client newQuery()
 * @method static Builder|Client query()
 * @method static Builder|Client updatedBy($userId)
 * @method static Builder|Client whereAddress($value)
 * @method static Builder|Client whereBirthday($value)
 * @method static Builder|Client whereContactParams($value)
 * @method static Builder|Client whereCreatedAt($value)
 * @method static Builder|Client whereCreatedBy($value)
 * @method static Builder|Client whereCustomMessage($value)
 * @method static Builder|Client whereDeletedAt($value)
 * @method static Builder|Client whereDeletedBy($value)
 * @method static Builder|Client whereDescription($value)
 * @method static Builder|Client whereEmail($value)
 * @method static Builder|Client whereFathersName($value)
 * @method static Builder|Client whereFirstName($value)
 * @method static Builder|Client whereGender($value)
 * @method static Builder|Client whereId($value)
 * @method static Builder|Client whereImportant($value)
 * @method static Builder|Client whereKalikratisVote($value)
 * @method static Builder|Client whereKapodistriasVote($value)
 * @method static Builder|Client whereLandline($value)
 * @method static Builder|Client whereLastName($value)
 * @method static Builder|Client whereMothersName($value)
 * @method static Builder|Client whereName($value)
 * @method static Builder|Client whereNamedayId($value)
 * @method static Builder|Client wherePhone($value)
 * @method static Builder|Client whereState($value)
 * @method static Builder|Client whereUpdatedAt($value)
 * @method static Builder|Client whereUpdatedBy($value)
 * @method static Builder|Client whereZipCode($value)
 * @mixin \Eloquent
 */
class Client extends Model
{
    use ModelTraits;
    use Notifiable;

    protected $fillable = ['first_name','last_name','gender','fathers_name','mothers_name','address','zip_code','state','landline','kalikratis_vote','kapodistrias_vote','custom_message', 'description', 'phone', 'email', 'contact_params', 'contact_via_sms', 'contact_via_email', 'birthday', 'nameday_id'];
    protected $attributes = [
        'contact_params' => '{"via_email":false,"via_sms":false}',
    ];
    protected $casts = [
        'contact_params' => 'array',
    ];
    protected $dates = ['birthday'];

    public function scopeBirthdayAt(Builder $query, $date = 'today')
    {
        $date = new Carbon($date);
        return $query->whereMonth('birthday', $date->month)
            ->whereDay('birthday', $date->day);
    }

    public function scopeNamedayAt(Builder $query, $date = 'today')
    {
        $date = new Carbon($date);
        return $query->whereHas('nameday', function (Builder $q) use ($date) {
            return $q->whereJsonContains('celebration_dates', $date->format(config('wishes.date_format')));
        });
    }

    /**
     * @return bool
     */
    public function getContactViaSmsAttribute()
    {
        return array_get($this->contact_params, 'via_sms');
    }

    /**
     * @param bool $value
     * @return bool
     */
    public function setContactViaSmsAttribute($value)
    {
        $p = $this->contact_params;
        $this->contact_params = array_set($p, 'via_sms', (bool)$value);
        return $this->contact_via_sms;
    }

    /**
     * @return bool
     */
    public function getContactViaEmailAttribute()
    {
        return array_get($this->contact_params, 'via_email');
    }

    /**
     * @param bool $value
     * @return bool
     */
    public function setContactViaEmailAttribute($value)
    {
        $p = $this->contact_params;
        $this->contact_params = array_set($p, 'via_email', (bool)$value);
        return $this->contact_via_email;
    }

    /**
     * @param mixed $notification
     * @return bool false if Notification was not sent because notification of this class was already sent today, true if notification was sent
     */
    public function notifySingle($notification)
    {
        if ($this->notifications()->whereDate('created_at', now())->where('type', get_class($notification))->exists()) {
            Log::info("Rejected Notification " . get_class($notification) . " because it is not single for {$this->name} #{$this->id}.");
            return false;
        }
        $this->notify($notification);
        return true;
    }

    /**
     * Route notifications for the Nexmo channel.
     *
     * @param \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        return (strlen($this->phone) == 10) ? "30{$this->phone}" : $this->phone;
    }

    public function notifications()
    {
        return $this->morphMany(ClientNotification::class, 'notifiable')->orderBy('created_at', 'desc');
    }

    /**
     * @return string
     */
    public function getNotificationMessageAttribute()
    {
        return $this->custom_message ?? __(config('wishes.message'), $this->getAttributes());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function client_notifications()
    {
        return $this->hasMany(ClientNotification::class, 'notifiable_id');
    }

    public function nameday()
    {
        return $this->belongsTo(Nameday::class);
    }

}
