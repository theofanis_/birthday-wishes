<?php

namespace App\Providers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laraning\NovaTimeField\TimeField;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use Nalingia\SimpleToolbarLink\SimpleToolbarLink;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    public function boot()
    {
        parent::boot();
        $this->settingsFields();
        Nova::translations(resource_path('lang/' . app()->getLocale() . '.json'));
    }

    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
//            ->withPasswordResetRoutes()
            ->register();
    }

    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return !is_null($user);
        });
    }

    protected function cards()
    {
        return [
            \Richardkeep\NovaTimenow\NovaTimenow::make()->timezones([config('app.timezone')]),
            \Llaski\NovaServerMetrics\Card::make(),
        ];
    }

    protected function dashboards()
    {
        return [];
    }

    public function tools()
    {
        if (Auth::user()->can('super-admin')) {
            return [
                \Epigra\NovaSettings\NovaSettingsTool::make(),
                \Davidpiesse\NovaPhpinfo\Tool::make(),
                \BinaryBuilds\NovaAdvancedCommandRunner\CommandRunner::make(),
                \Spatie\BackupTool\BackupTool::make(),
                \Insenseanalytics\NovaServerMonitor\NovaServerMonitor::make()->checks(['mysql']),
                \PhpJunior\NovaLogViewer\Tool::make(),
                \Sbine\RouteViewer\RouteViewer::make(),
                SimpleToolbarLink::make()->name('Telescope')->to('/telescope')->target('telescope')->icon('<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 80"><path fill="var(--sidebar-icon)" d="M0 40a39.87 39.87 0 0 1 11.72-28.28A40 40 0 1 1 0 40zm34 10a4 4 0 0 1-4-4v-2a2 2 0 1 0-4 0v2a4 4 0 0 1-4 4h-2a2 2 0 1 0 0 4h2a4 4 0 0 1 4 4v2a2 2 0 1 0 4 0v-2a4 4 0 0 1 4-4h2a2 2 0 1 0 0-4h-2zm24-24a6 6 0 0 1-6-6v-3a3 3 0 0 0-6 0v3a6 6 0 0 1-6 6h-3a3 3 0 0 0 0 6h3a6 6 0 0 1 6 6v3a3 3 0 0 0 6 0v-3a6 6 0 0 1 6-6h3a3 3 0 0 0 0-6h-3zm-4 36a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM21 28a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path></svg>'),
                SimpleToolbarLink::make()->name('Horizon')->to('/horizon')->target('horizon')->icon('<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path fill="var(--sidebar-icon)" d="M3.5,17.6C1.3,15.7,0,12.9,0,10c0-2.8,1.1-5.3,2.9-7.1C6.8-1,13.2-1,17.1,2.9s3.9,10.2,0,14.1 C13.4,20.8,7.5,21,3.5,17.6L3.5,17.6z M2.7,10.6c1.1-1,1.9-2.3,4-2.3c3.3,0,3.3,3.3,6.7,3.3c2.1,0,2.9-1.3,4-2.3 c-0.3-4-3.9-7-7.9-6.7S2.4,6.6,2.7,10.6L2.7,10.6z"/></svg>'),
            ];
        }
        return [
            \Epigra\NovaSettings\NovaSettingsTool::make()->canSeeWhen('update-settings'),
        ];
    }

    public function register()
    {
        //
    }

    private function settingsFields()
    {
        \Epigra\NovaSettings\NovaSettingsTool::addSettingsFields(function () {
            return [
                Textarea::make(__('Wish Message'), 'wish_message')->rules('required', 'max:167')->rows(2)->withMeta(['extraAttributes' => [
                    'placeholder' => __('Message that will be send to clients')],
                ])->help(__('Use placeholder <b>:name</b> to define the name of the client.')),
                TimeField::make(__('Send At'), 'send_at')->minuteIncrement(10)->rules('required')
                    ->resolveUsing(function ($value) {
                        return $value === null ? '' : Carbon::createFromFormat('H:i', $value)->format('H:i');
                    })->fillUsing(function (NovaRequest $request, $model, $attribute, $requestAttribute) {
                        $model->{$attribute} = $request->input($requestAttribute);
                    }),
                Text::make(__('Sender Name'), 'sender_name')->rules('required', 'max:11', 'alpha_num'),
                Text::make(__('Email Sender'), 'email_sender')->rules('required', 'email'),
                Text::make(__('Email Subject'), 'email_subject')->rules('required'),
                Text::make(__('Sendinblue API Key'), 'sendinblue_api_key')->help('<a href="https://account.sendinblue.com/advanced/api" target="_blank">⎋ account.sendinblue.com</a> v3 API KEY<br>Used for Email.'),
                Text::make(__('Nexmo/Vonage API Key'), 'nexmo_api_key')->help('<a href="https://dashboard.nexmo.com" target="_blank">⎋ dashboard.nexmo.com</a><br>Used for SMS.'),
                Text::make(__('Nexmo/Vonage API Secret'), 'nexmo_api_secret')->help('<a href="https://dashboard.nexmo.com" target="_blank">⎋ dashboard.nexmo.com</a><br>Used for SMS.'),
                Boolean::make(__('Service Enabled'), 'wishes_enabled')->help(__('Enable or disable the automated notification service!')),
            ];
        });
    }
}
