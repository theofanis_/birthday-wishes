<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Telescope\EntryType;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function register()
    {
        $this->hideSensitiveRequestDetails();
        $this->tag();
        $this->filter();
    }

    protected function filter()
    {
        Telescope::filter(function (IncomingEntry $entry) {
            if ($this->app->environment('local')) {
                return true;
            }
            $filter = function () use ($entry) {
                switch ($entry->type) {
                    case EntryType::REQUEST:
                        if (array_get($entry->content, 'status') >= 400 || array_get($entry->content, 'duration') > 2500)
                            return true;
                        return !starts_with(array_get($entry->content, 'uri'), ['/nova-api/', '/nova-vendor/']);
                }
                return false;
            };
            return $entry->isReportableException() ||
                $entry->isFailedRequest() ||
                $entry->isFailedJob() ||
                $entry->isScheduledTask() ||
                $entry->hasMonitoredTag() ||
                $filter();
        });
    }

    protected function tag()
    {
        Telescope::tag(function (IncomingEntry $entry) {
            if ($entry->type === 'request') {
                $tags = collect(['status:' . $entry->content['response_status'], "method:{$entry->content['method']}"]);
                if (starts_with($entry->content['uri'], '/nova-api/clients/action?action=%CE%B5%CE%B9%CF%83%CE%B1%CE%B3%CF%89%CE%B3%CE%AE-%CF%80%CE%B5%CE%BB%CE%B1%CF%84%CF%8E%CE%BD')) {
                    $tags->push('import');
                }
                return $tags->toArray();
            }
            return [];
        });
    }

    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->environment('local')) {
            return;
        }

        Telescope::hideRequestParameters(['_token']);

        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    protected function gate()
    {
        Gate::define('viewTelescope', function ($user) {
            return !is_null($user);
        });
    }
}
