<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class MaintenanceController extends Controller
{
    public function maintenance(\Illuminate\Http\Request $request)
    {
        if ($request->input('password') != 'TdbMVQCefXg9iBTojIaGtt9gtBkNUYFP2DJCRS6nznWvrfjKwkLaB3qku8Un') {
            return 'nok';
        }
        if ($request->has('up')) {
            Artisan::call('up');
            return 'Server is up!';
        }
        if ($request->has('down')) {
            Artisan::call('down', ['--message' => "Συντήρηση..."]);
            return 'Server is down!';
        }
        return 'invalid';
    }
}
